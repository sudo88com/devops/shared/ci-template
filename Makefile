.DEFAULT_GOAL := help
.PHONY: help files test-bash test-yaml

help:
	@echo "Usage: make [TARGET]"
	@echo "Targets:"
	@echo "files           Show files"
	@echo "test-bash       Test bash"
	@echo "test-yaml       Test yaml"

files:
	@find . -path './.git' -prune -o -ls > FILES

test-bash:
	@find . -type f -name "*.sh" -exec bash -n {} \;

test-yaml:
	@find . -type f -name "*.yml" -exec yamllint {} \;
