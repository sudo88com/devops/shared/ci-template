# GitLab CI/CD Template for Docker Buildx with ECR Public Registry

This repository provides a comprehensive GitLab CI/CD template for building and pushing Docker images using Docker Buildx to an AWS Elastic Container Registry (ECR) public repository. The template includes configurations for testing, building, and releasing Docker images in a GitLab environment.

## Features

- Automated Docker image builds using Docker Buildx.
- Push Docker images to a public AWS ECR repository.
- Environment-specific configurations.
- Secure handling of sensitive information with GitLab CI/CD secrets.
- Modular and reusable pipeline stages.

## Setup

To include this template in your GitLab CI/CD pipeline, add the following `include` statement in your `.gitlab-ci.yml` file:

```yaml
---
include:
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/docker-buildx-ecr-public/.test.yml'
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/docker-buildx-ecr-public/.build.yml'
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/docker-buildx-ecr-public/.main.yml'
```

To customize the behavior of your CI/CD pipeline and pass necessary environment variables to Docker, include the following variables in your `.gitlab-ci.yml` file:

```yaml
variables:
  CI_ENVIRONMENT_NAME: "dev"
  DOCKER_PLATFORM: "linux/amd64,linux/arm64"
  DOCKER_REGISTRY: "public.ecr.aws/<$ECR_ALIAS>/<$ECR_REPOSITORY_NAME>"
  DOCKER_LOGIN: "aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws"
  DOCKER_FILE_IMAGE: "Dockerfile"
  DOCKER_ADDITIONAL_ARG: ""
  # AWS_ACCESS_KEY_ID: ""
  # AWS_SECRET_ACCESS_KEY: ""
  # AWS_DEFAULT_REGION: ""
```

### Explanation of Variables

- CI_ENVIRONMENT_NAME: This variable is set to the name of the current GitLab environment, such as "dev", "staging", or "production".

- DOCKER_PLATFORM: Specifies the target platforms for the Docker Buildx build (e.g., linux/amd64,linux/arm64).

- DOCKER_REGISTRY: Specifies the AWS ECR registry URL where the Docker image will be pushed.

- DOCKER_LOGIN: Command to authenticate Docker to the AWS ECR registry using the AWS CLI.

- DOCKER_FILE_IMAGE: Specifies the Dockerfile to use for the build.

- DOCKER_ADDITIONAL_ARG: Any additional arguments to pass to the docker build command.

- AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION: These variables should be stored as GitLab CI/CD secrets to securely manage AWS credentials and region configuration.

### Storing AWS Credentials as GitLab CI/CD Secrets

1. Navigate to your project on GitLab.
2. Go to Settings > CI/CD > Variables.
3. Add the following variables:
    - `AWS_ACCESS_KEY_ID`: Your AWS access key ID.
    - `AWS_SECRET_ACCESS_KEY`: Your AWS secret access key.
    - `AWS_DEFAULT_REGION`: The default AWS region.

These variables will automatically be available to your pipeline jobs and can be referenced as needed in your Docker Buildx configuration.

## Example Project Structure

The `EXAMPLE` directory provides a sample project structure, including necessary Docker configurations. Here's a brief overview:

```
EXAMPLE
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
└── Dockerfile
```

### Key Files and Directories

- `.dockerignore`: Excludes files and directories from Docker builds to keep the image size smaller.
- `.gitignore`: Specifies files and directories Git should ignore, keeping the repository clean by excluding unnecessary files.
- `.gitlab-ci.yml`: Defines the CI/CD pipeline.
- `Dockerfile`: Contains the Docker build instructions.

## Usage

1. Configure GitLab CI/CD:

- Add the provided `include` statement to your `.gitlab-ci.yml`.
- Ensure your project structure aligns with the provided `EXAMPLE` directory.

2. Customize Docker Buildx and ECR Configurations:

- Modify the `Dockerfile` to suit your application.
- Update the CI/CD variables as needed in your `.gitlab-ci.yml`.

3. Store AWS Credentials:

- Add the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION` variables as GitLab CI/CD secrets.

4. Run the Pipeline:

- Commit and push your changes to trigger the GitLab CI/CD pipeline.
- Use Git tags to publish Docker images. Add a Git tag to your commit to trigger the build and release stage:

```bash
git tag -a v1.0.0 -m "Release version 1.0.0"
git push origin v1.0.0
```

5. Monitor the Pipeline:

- The pipeline will automatically run the test and build jobs.
- Docker images will be pushed to the AWS ECR repository upon successful builds.

## Security

A template could contain malicious code. For example, a template that contains the `export` shell command in a job
might accidentally expose secret project CI/CD variables in a job log.
If you're unsure if it's secure or not, you must ask security experts for cross-validation.

## Contribute CI/CD template merge requests

We welcome contributions to improve and enhance this CI/CD template. Please submit your merge requests with clear descriptions of the changes and their benefits. Ensure that your contributions align with the overall structure and purpose of this template.
