#!/usr/bin/env bash

set -o errexit -o pipefail

function clean_build_docker_buildx() {
    echo "Cleaning up..."
    docker buildx rm "mybuilder-${BUILDER_NAME}" 2>/dev/null || true
    docker logout "${DOCKER_REGISTRY}" 2>/dev/null || true
}

function define_var_required() {
    local variables="$@"
    for var in $variables; do
        if [ -n "${!var}" ]; then
            echo "$var=${!var}"
        else
            echo "Error: $var is not set" >&2
            exit 1
        fi
    done
}

function define_var_optional() {
    local variables="$@"
    for var in $variables; do
        if [ -n "${!var}" ]; then
            echo "$var=${!var}"
        else
            echo "Warning: $var is not set" >&2
        fi
    done
}

function define_apk() {
    local commands="$@"
    for cmd in $commands; do
        if command -v "$cmd" >/dev/null 2>&1; then
            echo "$cmd is already installed."
        else
            echo "$cmd is not installed. Installing..."
            apk add --no-cache "$cmd" || { echo "Error: Failed to install $cmd" >&2; exit 1; }
        fi
    done
}

function check_build_docker_buildx() {
    echo "Checking required variables..."
    define_var_required "AWS_ACCESS_KEY_ID" "AWS_SECRET_ACCESS_KEY" "AWS_DEFAULT_REGION" "CI_ENVIRONMENT_NAME" "DOCKER_LOGIN" "DOCKER_REGISTRY" "DOCKER_PLATFORM" "DOCKER_FILE_IMAGE"
    echo "Checking optional variables..."
    define_var_optional "DOCKER_ADDITIONAL_ARG"
    echo "Checking dependencies..."
    define_apk bash curl git aws-cli docker util-linux
    if ! aws --version >/dev/null 2>&1; then
        echo "Error: AWS CLI is not installed or not working" >&2
        exit 1
    fi
    if ! docker -v >/dev/null 2>&1; then
        echo "Error: Docker is not installed or not running" >&2
        exit 1
    fi
    docker info
}

function test_build_docker_buildx() {
    trap 'clean_build_docker_buildx' EXIT
    BUILDER_NAME=$(uuidgen)
    docker buildx create --use --name "mybuilder-${BUILDER_NAME}" >/dev/null || { echo "Error: Failed to create builder" >&2; exit 1; }
    docker buildx inspect
    docker buildx build --progress=plain --no-cache \
        --platform "${DOCKER_PLATFORM}" \
        -t "${DOCKER_REGISTRY}:test" \
        -f "$DOCKER_FILE_IMAGE" \
        $DOCKER_ADDITIONAL_ARG \
        . || { echo "Error: Failed to build Docker image" >&2; exit 1; }
    echo "Build complete!"
}

function create_ecr_public_repository() {
    ECR_REPOSITORY_NAME=$(echo "$DOCKER_REGISTRY" | sed -e 's|.*amazonaws.com/||')
    if ! aws ecr describe-repositories --repository-names "$ECR_REPOSITORY_NAME" > /dev/null 2>&1; then
        echo "Creating ECR repository $ECR_REPOSITORY_NAME..."
        aws ecr create-repository --repository-name "$ECR_REPOSITORY_NAME" || true
    else
        echo "ECR repository $ECR_REPOSITORY_NAME already exists."
    fi
}

function build_release_docker_buildx() {
    trap 'clean_build_docker_buildx' EXIT
    eval "${DOCKER_LOGIN}" || { echo "Error: Failed to log in to Docker registry" >&2; exit 1; }
    BUILDER_NAME=$(uuidgen)
    docker buildx create --use --name "mybuilder-${BUILDER_NAME}" >/dev/null || { echo "Error: Failed to create builder" >&2; exit 1; }
    docker buildx inspect
    create_ecr_public_repository
    docker buildx build --progress=plain --no-cache --push \
        --platform "${DOCKER_PLATFORM}" \
        --cache-to mode=max,image-manifest=true,oci-mediatypes=true,type=registry,ref="${DOCKER_REGISTRY}:cache" \
        --cache-from type=registry,ref="${DOCKER_REGISTRY}:cache" \
        -t "${DOCKER_REGISTRY}:${CI_COMMIT_REF_NAME#v}" \
        -t "${DOCKER_REGISTRY}:latest" \
        -f "$DOCKER_FILE_IMAGE" \
        $DOCKER_ADDITIONAL_ARG \
        . || { echo "Error: Failed to build and push Docker image" >&2; exit 1; }
    echo "Build complete!"
}

function build_stable_docker_buildx() {
    trap 'clean_build_docker_buildx' EXIT
    eval "${DOCKER_LOGIN}" || { echo "Error: Failed to log in to Docker registry" >&2; exit 1; }
    BUILDER_NAME=$(uuidgen)
    docker buildx create --use --name "mybuilder-${BUILDER_NAME}" >/dev/null || { echo "Error: Failed to create builder" >&2; exit 1; }
    docker buildx inspect
    create_ecr_public_repository
    docker buildx build --progress=plain --no-cache --push \
        --platform "${DOCKER_PLATFORM}" \
        --cache-to mode=max,image-manifest=true,oci-mediatypes=true,type=registry,ref="${DOCKER_REGISTRY}:cache" \
        --cache-from type=registry,ref="${DOCKER_REGISTRY}:cache" \
        -t "${DOCKER_REGISTRY}:stable-${CI_COMMIT_REF_NAME#v}" \
        -t "${DOCKER_REGISTRY}:stable" \
        -f "$DOCKER_FILE_IMAGE" \
        $DOCKER_ADDITIONAL_ARG \
        . || { echo "Error: Failed to build and push Docker image" >&2; exit 1; }
    echo "Build complete!"
}

if [ "$#" -eq 1 ]; then
    "$1"
else
    echo "Usage: $0 <function_name>" >&2
    exit 1
fi
