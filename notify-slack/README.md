# GitLab CI/CD Slack Notification Template

This repository contains a GitLab CI/CD template to send notifications to Slack. This template is useful for notifying your team about the status of your CI/CD pipelines directly in a Slack channel.

## Features

- Sends notifications to Slack for both successful and failed pipeline jobs.
- Includes details such as the committer, commit message, branch/tag, timestamp, and a link to the pipeline.

## Setup

To include this template in your GitLab CI/CD pipeline, add the following `include` statement in your `.gitlab-ci.yml` file:

```yaml
---
include:
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/notify-slack/.main.yml'
```

To customize the behavior of your CI/CD pipeline and pass necessary environment variables to Slack, include the following variables in your `.gitlab-ci.yml` file:

```yaml
variables:
  CI_ENVIRONMENT_NAME: "dev"
  # SLACK_WEBHOOK_URL: ""

after_script:
  - !reference [.notify:slack, script]
```

### Explanation of Variables

- CI_ENVIRONMENT_NAME: This variable is set to the name of the current GitLab environment, typically the branch or tag name of the commit. It ensures that the environment name dynamically reflects the commit reference.

- SLACK_WEBHOOK_URL: The Slack webhook URL for sending notifications.

### Storing SLACK_WEBHOOK_URL as GitLab CI/CD Secrets

1. Navigate to your project on GitLab.
2. Go to Settings > CI/CD > Variables.
3. Add the following variables:
    - `SLACK_WEBHOOK_URL`: The Slack webhook URL for sending notifications.

These variables will automatically be available to your pipeline jobs, and you can reference them as needed in your Slack configuration.

## Example Project Structure

The `EXAMPLE` directory provides a sample project structure, including necessary Slack configurations. Here's a brief overview:

```
EXAMPLE
├── .gitignore
├── .gitkeep
└── .gitlab-ci.yml
```

### Key Files and Directories

- `.gitlab-ci.yml` for defining the CI/CD pipeline.

## Usage

1. Configure GitLab CI/CD:

- Add the provided `include` statement to your `.gitlab-ci.yml`.
- Ensure your project structure aligns with the provided `EXAMPLE` directory.

2. Store SLACK_WEBHOOK_URL:

- Add the `SLACK_WEBHOOK_URL` variable as a GitLab CI/CD secret in Settings > CI/CD > Variables.

3. Run the Pipeline:

- Commit and push your changes to trigger the GitLab CI/CD pipeline.
- Monitor the pipeline for build, deploy, and destroy jobs.

## Security

A template could contain malicious code. For example, a template that contains the `export` shell command in a job
might accidentally expose secret project CI/CD variables in a job log.
If you're unsure if it's secure or not, you must ask security experts for cross-validation.

## Contribute CI/CD template merge requests

We welcome contributions to improve and enhance this CI/CD template. Please submit your merge requests with clear descriptions of the changes and their benefits. Ensure that your contributions align with the overall structure and purpose of this template.
