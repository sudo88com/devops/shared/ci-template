#!/usr/bin/env bash

set -o errexit -o pipefail

function define_var_required() {
    local variables="$@"
    for var in $variables; do
        if [ -n "${!var}" ]; then
            echo "$var=${!var}"
        else
            echo "Error: $var is not set" >&2
            exit 1
        fi
    done
}

function define_var_optional() {
    local variables="$@"
    for var in $variables; do
        if [ -n "${!var}" ]; then
            echo "$var=${!var}"
        else
            echo "Warning: $var is not set" >&2
        fi
    done
}

function define_apk() {
    local commands="$@"
    for cmd in $commands; do
        if command -v "$cmd" >/dev/null 2>&1; then
            echo "$cmd is already installed."
        else
            echo "$cmd is not installed. Installing..."
            apk add --no-cache "$cmd" || { echo "Error: Failed to install $cmd" >&2; exit 1; }
        fi
    done
}

function check_notify_slack() {
    echo "Checking required variables..."
    define_var_required "SLACK_WEBHOOK_URL" "CI_ENVIRONMENT_NAME"
    echo "Checking optional variables..."
    define_var_optional
    echo "Checking dependencies..."
    define_apk bash git curl jq
}

function send_notify_slack() {
    check_notify_slack
    local status="$CI_JOB_STATUS"
    local webhook_url="$SLACK_WEBHOOK_URL"
    local emoji
    local prefix
    if [ "$status" == "success" ]; then
        message="Deployment *succeeded* for project <$CI_PROJECT_URL|$CI_PROJECT_NAME>"
        emoji=":white_check_mark:"
    else
        message="Deployment *failed* for project <$CI_PROJECT_URL|$CI_PROJECT_NAME>"
        emoji=":x:"
    fi
    prefix="${emoji} *CI/CD Pipeline Notification*"
    local json_payload
    json_payload=$(jq -n \
        --arg prefix "$prefix" \
        --arg message "$message" \
        --arg committer "*Committer:* $GITLAB_USER_NAME" \
        --arg commit_msg "*Commit Message:* $CI_COMMIT_MESSAGE" \
        --arg branch_tag "*Branch/Tag:* $CI_COMMIT_REF_NAME" \
        --arg timestamp "*Timestamp:* $CI_COMMIT_TIMESTAMP" \
        --arg pipeline_url "*Pipeline URL:* <$CI_PIPELINE_URL|View Pipeline>" \
        '{ "blocks": [ { "type": "header", "text": { "type": "plain_text", "text": $prefix } }, { "type": "section", "text": { "type": "mrkdwn", "text": $message } }, { "type": "context", "elements": [ { "type": "mrkdwn", "text": $committer }, { "type": "mrkdwn", "text": $commit_msg }, { "type": "mrkdwn", "text": $branch_tag }, { "type": "mrkdwn", "text": $timestamp }, { "type": "mrkdwn", "text": $pipeline_url } ] } ] }')
    curl -s -X POST -H 'Content-type: application/json' --data "$json_payload" "$webhook_url" || { echo "Error: Failed to send Slack notification" >&2; exit 1; }
}

function notify_slack() {
    check_notify_slack
    send_notify_slack
}

if [ "$#" -eq 1 ]; then
    "$1"
else
    echo "Usage: $0 <function_name>" >&2
    exit 1
fi
