## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 5.39.1 |

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_CI_ENVIRONMENT_NAME"></a> [CI\_ENVIRONMENT\_NAME](#input\_CI\_ENVIRONMENT\_NAME) | Project Environment (Required) | `string` | `"dev"` | no |

## Outputs

No outputs.
