
variable "CI_ENVIRONMENT_NAME" {
  type        = string
  description = "Project Environment (Required)"
  default     = "dev"
}
