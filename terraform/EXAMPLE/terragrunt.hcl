
locals {
  aws_region          = ""
  aws_bucket          = ""
  aws_dynamodb_table  = ""
  aws_project_name    = ""
  ci_environment_name = get_env("TF_VAR_CI_ENVIRONMENT_NAME")
  inputs_from_tfvars  = jsondecode(read_tfvars_file("${local.ci_environment_name}.tfvars"))
}

remote_state {
  backend = "s3"
  generate = {
    path      = "remote_state.tf"
    if_exists = "overwrite"
  }
  config = {
    region         = local.aws_region
    bucket         = local.aws_bucket
    dynamodb_table = local.aws_dynamodb_table
    key            = "${get_aws_account_id()}/${local.aws_region}/${local.aws_project_name}/${local.ci_environment_name}/${path_relative_to_include()}/terraform.tfstate"
    encrypt        = true
  }
}
