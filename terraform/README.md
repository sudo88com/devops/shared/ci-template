# GitLab CI/CD Template for Terraform (Terragrunt)

This repository provides a comprehensive GitLab CI/CD template for managing Terraform deployments. The template includes configurations for building, deploying, and destroying Terraform infrastructure using Terragrunt. The structure and scripts included are designed to automate and streamline the Terraform workflow in a GitLab environment.

## Features

- Automated Terraform initialization, planning, and application using Terragrunt.
- Environment-specific configurations.
- Secure handling of sensitive information with GitLab CI/CD secrets.
- Modular and reusable Terraform configurations.

## Setup

To include this template in your GitLab CI/CD pipeline, add the following `include` statement in your `.gitlab-ci.yml` file:

```yaml
---
include:
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/terraform/.build.yml'
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/terraform/.deploy.yml'
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/terraform/.destroy.yml'
  - 'https://gitlab.com/sudo88com/devops/shared/ci-template/raw/main/terraform/.main.yml'
```

To customize the behavior of your CI/CD pipeline and pass necessary environment variables to Terraform, include the following variables in your `.gitlab-ci.yml` file:

```yaml
variables:
  CI_ENVIRONMENT_NAME: $CI_COMMIT_REF_NAME
  TF_VAR_CI_ENVIRONMENT_NAME: $CI_ENVIRONMENT_NAME
  TF_VAR_CONFIG: "$TF_VAR_CI_ENVIRONMENT_NAME.tfvars"
  TF_ROOT: $CI_PROJECT_DIR
  # AWS_ACCESS_KEY_ID: ""
  # AWS_SECRET_ACCESS_KEY: ""
  # AWS_DEFAULT_REGION: ""
```

### Explanation of Variables

- CI_ENVIRONMENT_NAME: This variable is set to the name of the current GitLab environment, typically the branch or tag name of the commit. It ensures that the environment name dynamically reflects the commit reference.

- TF_VAR_CI_ENVIRONMENT_NAME: This variable is passed to Terraform as an environment variable, making the CI environment name available to Terraform configurations.

- TF_VAR_CONFIG: This variable specifies the Terraform variable file to use based on the current environment. For example, if the environment name is dev, the corresponding dev.tfvars file will be used.

- TF_ROOT: This variable sets the root directory for Terraform, pointing to the project directory in the GitLab CI/CD environment.

- AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION: These variables should be store as GitLab CI/CD secrets. This ensures that your sensitive information is kept secure and not hardcoded in the pipeline configuration..

### Storing AWS Credentials as GitLab CI/CD Secrets

1. Navigate to your project on GitLab.
2. Go to Settings > CI/CD > Variables.
3. Add the following variables:
    - `AWS_ACCESS_KEY_ID`: Your AWS access key ID.
    - `AWS_SECRET_ACCESS_KEY`: Your AWS secret access key.
    - `AWS_DEFAULT_REGION`: The default AWS region.

These variables will automatically be available to your pipeline jobs, and you can reference them as needed in your Terraform configuration.

## Example Project Structure

The `EXAMPLE` directory provides a sample project structure, including necessary Terraform and Terragrunt configurations. Here's a brief overview:

```
EXAMPLE
├── .gitignore
├── .gitkeep
├── .gitlab-ci.yml
├── Makefile
├── README.md
├── dev.tfvars
├── modules
│   └── vpc
│       ├── README.md
│       ├── _locals.tf
│       ├── _main.tf
│       ├── _output.tf
│       ├── _variable.tf
│       ├── dev.tfvars
│       ├── pre.tfvars
│       └── prod.tfvars
├── pre.tfvars
├── prod.tfvars
└── terragrunt.hcl
```

### Key Files and Directories

- `.gitlab-ci.yml` for defining the CI/CD pipeline.
- `Makefile` for build automation.
- `dev.tfvars`, `pre.tfvars`, `prod.tfvars` for environment-specific variables.
- `modules/vpc` for modular Terraform configurations.
- `terragrunt.hcl` for Terragrunt configurations.

## Usage

1. Configure GitLab CI/CD:

- Add the provided `include` statement to your `.gitlab-ci.yml`.
- Ensure your project structure aligns with the provided `EXAMPLE` directory.

2. Customize Terraform and Terragrunt:

- Modify the `*.tfvars` files to suit your environment configurations.
- Update the Terragrunt configuration as needed.

3. Store AWS Credentials:

- Add the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_DEFAULT_REGION` variables as GitLab CI/CD secrets.

4. Run the Pipeline:

- Commit and push your changes to trigger the GitLab CI/CD pipeline.
- Monitor the pipeline for build, deploy, and destroy jobs.

## Security

A template could contain malicious code. For example, a template that contains the `export` shell command in a job
might accidentally expose secret project CI/CD variables in a job log.
If you're unsure if it's secure or not, you must ask security experts for cross-validation.

## Contribute CI/CD template merge requests

We welcome contributions to improve and enhance this CI/CD template. Please submit your merge requests with clear descriptions of the changes and their benefits. Ensure that your contributions align with the overall structure and purpose of this template.
