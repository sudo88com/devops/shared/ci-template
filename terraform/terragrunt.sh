#!/usr/bin/env bash

set -o errexit -o pipefail

function define_var_required() {
    local variables="$@"
    for var in $variables; do
        if [ -n "${!var}" ]; then
            echo "$var=${!var}"
        else
            echo "Error: $var is not set" >&2
            exit 1
        fi
    done
}

function define_var_optional() {
    local variables="$@"
    for var in $variables; do
        if [ -n "${!var}" ]; then
            echo "$var=${!var}"
        else
            echo "Warning: $var is not set" >&2
        fi
    done
}

function define_apk() {
    local commands="$@"
    for cmd in $commands; do
        if command -v "$cmd" >/dev/null 2>&1; then
            echo "$cmd is already installed."
        else
            echo "$cmd is not installed. Installing..."
            apk add "$cmd" || { echo "Error: Failed to install $cmd" >&2; exit 1; }
        fi
    done
}

function check_terragrunt() {
    echo "Checking required variables..."
    define_var_required "AWS_ACCESS_KEY_ID" "AWS_SECRET_ACCESS_KEY" "AWS_DEFAULT_REGION"
    echo "Checking optional variables..."
    define_var_optional "TF_ROOT" "TF_VAR_CONFIG"
    echo "Checking dependencies..."
    define_apk bash curl aws-cli jq terraform terragrunt
    if ! terraform -v >/dev/null 2>&1; then
        echo "Error: Terraform is not installed or not working" >&2
        exit 1
    fi
    if ! terragrunt -v >/dev/null 2>&1; then
        echo "Error: Terragrunt is not installed or not working" >&2
        exit 1
    fi
    terragrunt run-all init -upgrade || { echo "Error: Failed to initialize Terragrunt" >&2; exit 1; }
}

function build_terragrunt_all() {
    check_terragrunt
    cd "${TF_ROOT:-$CI_PROJECT_DIR}" || { echo "Error: Failed to change directory" >&2; exit 1; }
    VAR_FILE_ARG="-var-file=${TF_VAR_CONFIG}"
    terragrunt run-all refresh ${TF_VAR_CONFIG:+"$VAR_FILE_ARG"} || { echo "Error: Failed to refresh Terraform state" >&2; exit 1; }
    terragrunt run-all plan ${TF_VAR_CONFIG:+"$VAR_FILE_ARG"} || { echo "Error: Failed to plan Terraform changes" >&2; exit 1; }
}

function deploy_terragrunt_all() {
    check_terragrunt
    cd "${TF_ROOT:-$CI_PROJECT_DIR}" || { echo "Error: Failed to change directory" >&2; exit 1; }
    VAR_FILE_ARG="-var-file=${TF_VAR_CONFIG}"
    terragrunt run-all refresh ${TF_VAR_CONFIG:+"$VAR_FILE_ARG"} || { echo "Error: Failed to refresh Terraform state" >&2; exit 1; }
    terragrunt run-all apply ${TF_VAR_CONFIG:+"$VAR_FILE_ARG"} -auto-approve --terragrunt-non-interactive || { echo "Error: Failed to apply Terraform changes" >&2; exit 1; }
}

function destroy_terragrunt_all() {
    check_terragrunt
    cd "${TF_ROOT:-$CI_PROJECT_DIR}" || { echo "Error: Failed to change directory" >&2; exit 1; }
    VAR_FILE_ARG="-var-file=${TF_VAR_CONFIG}"
    terragrunt run-all refresh ${TF_VAR_CONFIG:+"$VAR_FILE_ARG"} -lock=false || { echo "Error: Failed to refresh Terraform state" >&2; exit 1; }
    terragrunt run-all destroy ${TF_VAR_CONFIG:+"$VAR_FILE_ARG"} -auto-approve --terragrunt-non-interactive -lock=false || { echo "Error: Failed to destroy Terraform resources" >&2; exit 1; }
}

if [ "$#" -eq 1 ]; then
    "$1"
else
    echo "Usage: $0 <function_name>" >&2
    exit 1
fi
